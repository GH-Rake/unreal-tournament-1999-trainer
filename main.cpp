//Credits GuidedHacking.com
#include <Windows.h>
#include <iostream>
#include <string>
#include <ctime>
#include <tlhelp32.h>
#include <tchar.h>
using namespace std;

//function prototypes
DWORD_PTR dwGetModuleBaseAddress(DWORD dwProcID, TCHAR *szModuleName);
DWORD CalculatePointer(HANDLE hProcHandle, int PointerLevel, DWORD Offsets[], DWORD BaseAddress);
void UpdateAddresses(HANDLE hProcHandle);
void WriteToMemoryContinuously(HANDLE hProcHandle);
void UpdateDisplay2();

LPCSTR LGameWindow = "Unreal Tournament";
string GameStatus;
bool bGameAvail;
bool bUpdateDisplay;

//Player Class Related
DWORD CoreBaseAddress;

DWORD PawnBasePointer;
DWORD PawnBasePointerOffsets[] = { 0x18, 0x30, 0x0 };
DWORD PawnBaseAddress;//this is offset 0x0 of Pawn Class

DWORD RunSpeedAddress;
DWORD RunSpeedOffset = 0x26C;
BYTE RunSpeedValueHacked[] = { 0x00, 0x00, 0x7a, 0x44 };//1000 in decimal
BYTE RegRunSpeedValue[4];
bool bRunSpeedStatus;
string sRunSpeedStatus = "OFF";

DWORD HealthAddress;
DWORD HealthOffset = 0x31C;
BYTE HackedHealthValue[] = { 0x78, 0x03, 0x0, 0x0 }; //888 in decimal
BYTE RegHealthValue[4];
bool bHealthStatus;
string sHealthStatus = "OFF";

DWORD OneShotKillAddress;
DWORD OneShotKillOffset = 0x3C8;
BYTE OneShotKillValueRegular[4];
BYTE OneShotKillValueHacked[4] = { 0x00, 0x00, 0x5E, 0x44 };
bool bOneShotKillStatus;
string sOneShotKillStatus = "OFF";

DWORD JumpHeightAddress;
DWORD JumpHeightOffset = 0x27C;
BYTE HackedJumpHeightValue[] = { 0x00, 0x0C, 0x32, 0x44 }; //715 in decimal
BYTE RegJumpHeightValue[4];
bool bJumpHeightStatus;
string sJumpHeightStatus = "OFF";

DWORD XLocAddress;
DWORD YLocAddress;
DWORD ZLocAddress;
DWORD XLocOffset = 0xd0;
DWORD YLocOffset = 0xd4;
DWORD ZLocOffset = 0xd8;
FLOAT XLocValue;
FLOAT YLocValue;
FLOAT ZLocValue;

//PRI class related
DWORD PRIPointer = { 0x10590550 };
DWORD PRIPointerOffsets[3] = { 0x30, 0x44c, 0x0 };
DWORD PRIBaseAddress;

DWORD NumOfKillsAddress;
DWORD NumOfKillsOffset = 0x23c;
BYTE HackedNumoOfKillsValue[4] = { 0x0, 0x0, 0x5E, 0x44 };//888 in decimal
float HackedNumoOfKillsValueFloat;
BYTE RegNumOfKillsValue[4];
bool bNumOfKillsStatus;
string sNumOfKillsStatus = "OFF";

DWORD TeamAddress;
DWORD TeamOffset = 0x234;
BYTE HackedTeamValue[] = { 0x00, 0x01, 0x02, 0x03 };// 0=red 1=blue 2=green 3=gold
BYTE CurrentTeamValue;
string sTeamStatus = "---";

//Inventory class related
DWORD AmmoPointer = 0x10B3E014;
DWORD AmmoPointerOffsets[] = { 0x2f4, 0x300 };
DWORD AmmoAddress;
BYTE AmmoValueHacked[] = { 0x78, 0x03, 0x0, 0x0 };
bool bAmmoStatus;
string sAmmoStatus = "OFF";

int main()
{
	SetConsoleTitle("AnomanderRake's UT99 Trainer");
	system("mode 53, 30");
	HWND hGameWindow = NULL;
	DWORD dwProcId = NULL;
	HANDLE hProcHandle = NULL;
	int timeSinceLastUpdate = clock();
	int GameAvailTMR = clock();
	int KeyPressTimer = clock();
	bUpdateDisplay = true;

	while (!GetAsyncKeyState(VK_INSERT)) //Main program loop
	{
		if (clock() - GameAvailTMR > 300)
		{
			GameAvailTMR = clock();
			bGameAvail = false;
			hGameWindow = FindWindow(NULL, LGameWindow);
			if (hGameWindow)
			{
				GetWindowThreadProcessId(hGameWindow, &dwProcId);

				if (dwProcId)
				{
					hProcHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcId);
					if (hProcHandle == INVALID_HANDLE_VALUE || hProcHandle == NULL)
					{
						GameStatus = "Can't open process";
					}
					else
					{
						GameStatus = "Process ID Found UT99 Running";
						bGameAvail = true;
						CoreBaseAddress = dwGetModuleBaseAddress(dwProcId, _T("Core.dll"));
						UpdateAddresses(hProcHandle);//Update Addresses to keep them relevant
					}
				}
				else GameStatus = "Failed to get procid";
			}
			else GameStatus = "Unreal Tournament not running";

			//if UpdateNextRun is or 5 seconds have passed without an update
			if (bUpdateDisplay || clock() - timeSinceLastUpdate > 3000)
			{
				UpdateDisplay2();
				system("cls");
				cout << "----------------------------------------------------" << endl;
				cout << "                GuidedHacking.com                   " << endl;
				cout << "           AnomanderRake's UT99 Trainer             " << endl;
				cout << "----------------------------------------------------" << endl << endl;
				cout << "GAME STATUS: " << GameStatus << "   " << endl << endl;
				cout << "[NUMPAD1] Invincibility              -> " << sHealthStatus << " <-" << endl << endl;
				cout << "[NUMPAD2] Set 2.5X RunSpeed          -> " << sRunSpeedStatus << " <-" << endl << endl;
				cout << "[NUMPAD3] Set 2x Jump Height         -> " << sJumpHeightStatus << " <-" << endl << endl;
				cout << "[NUMPAD4] Set # of kills to 888      -> " << sNumOfKillsStatus << " <-" << endl << endl;
				cout << "[NUMPAD5] Set Teleporter to coords" << endl << endl;
				cout << "[NUMPAD6] Teleport to set coords" << endl << endl;
				cout << "[NUMPAD7] Change Team -  Current Team-> " << sTeamStatus << " <-" << endl << endl;
				cout << "[NUMPAD8] Unlimited Ammo             -> " << sAmmoStatus << " <-" << endl << endl;
				cout << "[NUMPAD9] One Shot Kills             -> " << sOneShotKillStatus << " <-" << endl << endl;
				cout << "[INSERT] Exit" << endl << endl;
				cout << "---------------------------------------------------" << endl;

				bUpdateDisplay = false;
				timeSinceLastUpdate = clock();
			}

			if (bGameAvail)
			{
				WriteToMemoryContinuously(hProcHandle);
			}
		}

		if (clock() - KeyPressTimer > 200)
		{
			if (bGameAvail)
			{
				if (GetAsyncKeyState(VK_NUMPAD1))
				{
					KeyPressTimer = clock();
					bHealthStatus = !bHealthStatus;
					bUpdateDisplay = true;
					if (bHealthStatus)
					{
						sHealthStatus = "ON";
					}
					else
					{
						sHealthStatus = "OFF";
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD2))
				{
					KeyPressTimer = clock();
					bRunSpeedStatus = !bRunSpeedStatus;
					bUpdateDisplay = true;
					if (bRunSpeedStatus)
					{
						ReadProcessMemory(hProcHandle, (LPCVOID)RunSpeedAddress, &RegRunSpeedValue, 4, NULL);//read unhacked value
						WriteProcessMemory(hProcHandle, (BYTE*)RunSpeedAddress, &RunSpeedValueHacked, 4, NULL); //write hacked value
						sRunSpeedStatus = "ON";
					}
					else
					{
						WriteProcessMemory(hProcHandle, (BYTE*)RunSpeedAddress, &RegRunSpeedValue, 4, NULL); //revert to unhacked value
						sRunSpeedStatus = "OFF";
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD3))
				{
					KeyPressTimer = clock();
					bJumpHeightStatus = !bJumpHeightStatus;
					bUpdateDisplay = true;
					if (bJumpHeightStatus)
					{
						ReadProcessMemory(hProcHandle, (LPCVOID)JumpHeightAddress, &RegJumpHeightValue, 4, NULL);//read unhacked value
						WriteProcessMemory(hProcHandle, (BYTE*)JumpHeightAddress, &HackedJumpHeightValue, sizeof(HackedJumpHeightValue), NULL); //write hacked value
						sJumpHeightStatus = "ON";
					}
					else
					{
						WriteProcessMemory(hProcHandle, (BYTE*)JumpHeightAddress, &RegJumpHeightValue, sizeof(HackedJumpHeightValue), NULL); //revert to unhacked value
						sJumpHeightStatus = "OFF";
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD4))
				{
					KeyPressTimer = clock();
					bNumOfKillsStatus = !bNumOfKillsStatus;
					bUpdateDisplay = true;
					if (bNumOfKillsStatus)
					{
						ReadProcessMemory(hProcHandle, (LPCVOID)NumOfKillsAddress, &RegNumOfKillsValue, 4, NULL);//read unhacked value
						WriteProcessMemory(hProcHandle, (BYTE*)NumOfKillsAddress, &HackedNumoOfKillsValue, 4, NULL); //write hacked value
						sNumOfKillsStatus = "ON";
					}
					else
					{
						WriteProcessMemory(hProcHandle, (BYTE*)NumOfKillsAddress, &RegNumOfKillsValue, 4, NULL); //revert
						sNumOfKillsStatus = "OFF";
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD5))
				{
					KeyPressTimer = clock();
					bUpdateDisplay = true;

					//Read current coordinates
					ReadProcessMemory(hProcHandle, (LPCVOID)XLocAddress, &XLocValue, 4, NULL);
					ReadProcessMemory(hProcHandle, (LPCVOID)YLocAddress, &YLocValue, 4, NULL);
					ReadProcessMemory(hProcHandle, (LPCVOID)ZLocAddress, &ZLocValue, 4, NULL);
				}

				else if (GetAsyncKeyState(VK_NUMPAD6))
				{
					KeyPressTimer = clock();
					bUpdateDisplay = true;

					//Write saved coordinates
					WriteProcessMemory(hProcHandle, (void *)XLocAddress, &XLocValue, sizeof(XLocValue), 0);
					WriteProcessMemory(hProcHandle, (void *)YLocAddress, &YLocValue, sizeof(YLocValue), 0);
					WriteProcessMemory(hProcHandle, (void *)ZLocAddress, &ZLocValue, sizeof(ZLocValue), 0);
				}

				else if (GetAsyncKeyState(VK_NUMPAD7))
				{
					KeyPressTimer = clock();
					bUpdateDisplay = true;

					//reads current team value and cycles to next team with each key press
					ReadProcessMemory(hProcHandle, (BYTE*)TeamAddress, &CurrentTeamValue, 1, NULL);
					if (CurrentTeamValue == 0x00)
					{
						WriteProcessMemory(hProcHandle, (BYTE*)TeamAddress, &HackedTeamValue[1], 1, NULL); //write blue
					}
					else if (CurrentTeamValue == 0x01)
					{
						WriteProcessMemory(hProcHandle, (BYTE*)TeamAddress, &HackedTeamValue[2], 1, NULL); //write green
					}
					else if (CurrentTeamValue == 0x02)
					{
						WriteProcessMemory(hProcHandle, (BYTE*)TeamAddress, &HackedTeamValue[3], 1, NULL); //write gold
					}
					else if (CurrentTeamValue == 0x03)
					{
						WriteProcessMemory(hProcHandle, (BYTE*)TeamAddress, &HackedTeamValue[0], 1, NULL); //write red
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD8))
				{
					KeyPressTimer = clock();
					bAmmoStatus = !bAmmoStatus;
					bUpdateDisplay = true;
					if (bAmmoStatus)
					{
						sAmmoStatus = "ON";
					}
					else
					{
						sAmmoStatus = "OFF";
					}
				}

				else if (GetAsyncKeyState(VK_NUMPAD9))
				{
					KeyPressTimer = clock();
					bOneShotKillStatus = !bOneShotKillStatus;
					bUpdateDisplay = true;
					if (bOneShotKillStatus)
					{
						ReadProcessMemory(hProcHandle, (LPCVOID)OneShotKillAddress, &OneShotKillValueRegular, 4, NULL);//read reg value
						WriteProcessMemory(hProcHandle, (BYTE*)OneShotKillAddress, &OneShotKillValueHacked, 4, NULL);//write hacked value
						sOneShotKillStatus = "ON";
					}
					else
					{
						WriteProcessMemory(hProcHandle, (BYTE*)OneShotKillAddress, &OneShotKillValueRegular, 4, NULL); //revert
						sOneShotKillStatus = "OFF";
					}
				}
			}
		}
	}//end of main program loop

	CloseHandle(hProcHandle);
	CloseHandle(hGameWindow);

	return ERROR_SUCCESS;
}

//Function declarations
DWORD CalculatePointer(HANDLE hProcHandle, int PointerLevel, DWORD Offsets[], DWORD BaseAddress)
{
	DWORD Pointer = BaseAddress;
	DWORD TempBuffer;

	DWORD PointerAddress;
	for (int i = 0; i < PointerLevel; i++)
	{
		if (i == 0)
		{
			ReadProcessMemory(hProcHandle, (LPCVOID)Pointer, &TempBuffer, 4, NULL);
		}

		PointerAddress = TempBuffer + Offsets[i];
		ReadProcessMemory(hProcHandle, (LPCVOID)PointerAddress, &TempBuffer, 4, NULL);
	}
	return PointerAddress;
}

void UpdateAddresses(HANDLE hProcHandle)
{
	//Actor.Pawn class related adresses
	PawnBasePointer = CoreBaseAddress + 0xE56B0;
	PawnBaseAddress = CalculatePointer(hProcHandle, 3, PawnBasePointerOffsets, PawnBasePointer);
	HealthAddress = PawnBaseAddress + HealthOffset;
	RunSpeedAddress = PawnBaseAddress + RunSpeedOffset;
	JumpHeightAddress = PawnBaseAddress + JumpHeightOffset;
	OneShotKillAddress = PawnBaseAddress + OneShotKillOffset;
	XLocAddress = PawnBaseAddress + XLocOffset;
	YLocAddress = PawnBaseAddress + YLocOffset;
	ZLocAddress = PawnBaseAddress + ZLocOffset;

	//Player replication info class Related Addresses
	PRIBaseAddress = CalculatePointer(hProcHandle, 3, PRIPointerOffsets, PRIPointer);
	NumOfKillsAddress = PRIBaseAddress + NumOfKillsOffset;
	TeamAddress = PRIBaseAddress + TeamOffset;
	ReadProcessMemory(hProcHandle, (BYTE*)TeamAddress, &CurrentTeamValue, 1, NULL);//read current team value for display

																				   //Inventory.Ammo class struct
	AmmoAddress = CalculatePointer(hProcHandle, 2, AmmoPointerOffsets, AmmoPointer);
}

void WriteToMemoryContinuously(HANDLE hProcHandle)
{
	if (bHealthStatus)
	{
		WriteProcessMemory(hProcHandle, (BYTE*)HealthAddress, &HackedHealthValue, sizeof(HackedHealthValue), NULL);
	}
	if (bAmmoStatus)
	{
		AmmoAddress = CalculatePointer(hProcHandle, 2, AmmoPointerOffsets, AmmoPointer);
		WriteProcessMemory(hProcHandle, (BYTE*)AmmoAddress, &AmmoValueHacked, 4, NULL);
	}
}

void UpdateDisplay2()
{
	if (CurrentTeamValue == 0x00)
	{
		sTeamStatus = "Red";
	}
	else if (CurrentTeamValue == 0x01)
	{
		sTeamStatus = "Blue";
	}
	else if (CurrentTeamValue == 0x02)
	{
		sTeamStatus = "Green";
	}
	else if (CurrentTeamValue == 0x03)
	{
		sTeamStatus = "Gold";
	}
}

DWORD_PTR dwGetModuleBaseAddress(DWORD dwProcID, TCHAR *szModuleName)
{
	DWORD_PTR dwModuleBaseAddress = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, dwProcID);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		MODULEENTRY32 ModuleEntry32;
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
		if (Module32First(hSnapshot, &ModuleEntry32))
		{
			do
			{
				if (_tcsicmp(ModuleEntry32.szModule, szModuleName) == 0)
				{
					dwModuleBaseAddress = (DWORD_PTR)ModuleEntry32.modBaseAddr;
					break;
				}
			} while (Module32Next(hSnapshot, &ModuleEntry32));
		}
		CloseHandle(hSnapshot);
	}
	return dwModuleBaseAddress;
}