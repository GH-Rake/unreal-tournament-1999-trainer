# Unreal Tournament 1999 Trainer

![eIz6034[1].png](https://bitbucket.org/repo/8BbKbn/images/1115677666-eIz6034%5B1%5D.png)

## What does this do?

This is an external single player trainer that gives you unlimited health, ammo and more.

## Why?

This was my first hack I ever made and my favorite game of all time.

## Usage

Compile with multi byte character set enabled.  Start game, start hack + have fun.

## TODO

Nothing, this project is retired and exists to remind myself of humbler beginnings.
Really want to do an unreal engine game tho soon

## Development History

This was my first hack I made as a branch from Fleep's Assault Cube trainer in 2014.  I started with unlimited health and ammo and practiced reverse engineering to add 7 more functions.
I had an external aimbot for this game also but lost it :(

## Links

[UT 99 External Trainer Thread](http://guidedhacking.com/showthread.php?5783-Unreal-Tournament-1999-8-Trainer)